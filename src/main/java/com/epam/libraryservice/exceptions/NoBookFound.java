package com.epam.libraryservice.exceptions;

@SuppressWarnings("serial")
public class NoBookFound extends RuntimeException{
	public NoBookFound(String message) {
		super(message);
	}
}
