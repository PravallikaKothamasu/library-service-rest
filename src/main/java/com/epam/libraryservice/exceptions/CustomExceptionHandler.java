package com.epam.libraryservice.exceptions;

import java.util.ArrayList;
import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;

@ControllerAdvice
@RestControllerAdvice
public class CustomExceptionHandler {

//	@ExceptionHandler(AlreadyIssuedException.class)
//	public final ResponseEntity<ErrorResponse> handleAlreadyIssuedException(AlreadyIssuedException ex,
//			WebRequest request) {
//		List<String> details = new ArrayList<>();
//		details.add(ex.getLocalizedMessage());
//		ErrorResponse error = new ErrorResponse("INCORRECT_REQUEST", details);
//		return new ResponseEntity<>(error, HttpStatus.NOT_FOUND);
//	}
	
	@ExceptionHandler(NoSuchBookIssued.class)
	public final ResponseEntity<ErrorResponse> handleNoSuchBookIssued(NoSuchBookIssued ex,
			WebRequest request) {
		List<String> details = new ArrayList<>();
		details.add(ex.getLocalizedMessage());
		ErrorResponse error = new ErrorResponse("INCORRECT_REQUEST", details);
		return new ResponseEntity<>(error, HttpStatus.NOT_FOUND);
	}

}
