package com.epam.libraryservice.exceptions;

@SuppressWarnings("serial")
public class AlreadyIssuedException extends RuntimeException{
	public AlreadyIssuedException(String message) {
		super(message);
	}
}
