package com.epam.libraryservice.exceptions;

@SuppressWarnings("serial")
public class NoSuchBookIssued extends RuntimeException{
	public NoSuchBookIssued(String message) {
		super(message);
	}
}
