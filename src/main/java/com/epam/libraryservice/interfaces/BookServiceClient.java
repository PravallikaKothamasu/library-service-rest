package com.epam.libraryservice.interfaces;

import java.util.List;
import java.util.Optional;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;

import com.epam.libraryservice.fallbackfactory.BookServiceFallBackFactory;
import com.epam.libraryservice.model.Book;
import com.epam.libraryservice.model.BookDTO;

@Component
@FeignClient(name = "book-service", fallbackFactory = BookServiceFallBackFactory.class)
public interface BookServiceClient {

	@GetMapping("/books")
	ResponseEntity<List<Book>> getBooksByLibraryService();

	@GetMapping("/books/{bookId}")
	public ResponseEntity<Book> getBookByIdByLibraryService(@PathVariable int bookId);

	@PostMapping("/books")
	public ResponseEntity<Book> addBookByLibraryService(@RequestBody BookDTO bookToBeAdded);

	@DeleteMapping("/books/{bookId}")
	public ResponseEntity<?> deleteBookByIdByLibraryService(@PathVariable int bookId);

	@PutMapping("/books/{bookId}")
	public ResponseEntity<Book> updateBookByLibraryService(@PathVariable int bookId,
			@RequestBody BookDTO bookToBeUpdated);
}
