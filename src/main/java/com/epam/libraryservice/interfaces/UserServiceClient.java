package com.epam.libraryservice.interfaces;

import java.util.List;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;

import com.epam.libraryservice.fallbackfactory.UserServiceFallBackFactory;
import com.epam.libraryservice.model.User;
import com.epam.libraryservice.model.UserDTO;

@FeignClient(name = "user-service",fallbackFactory = UserServiceFallBackFactory.class)
@Component
public interface UserServiceClient {

	@GetMapping("/users")
	public ResponseEntity<List<User>> getAllUsersByLibraryService();

	@GetMapping("/users/{userId}")
	public ResponseEntity<User> getUserByIdByLibraryService(@PathVariable int userId);

	@PostMapping("/users")
	@CrossOrigin(origins = "http://localhost:4200")
	public ResponseEntity<User> addUserByLibraryService(@RequestBody UserDTO userDTO);

	@PutMapping("/users/{userId}")
	public ResponseEntity<User> updateUserByLibraryService(@PathVariable int userId, @RequestBody UserDTO userDTO);

	@DeleteMapping("/users/{userId}")
	public ResponseEntity<?> deleteUserByLibraryService(@PathVariable int userId);
}
