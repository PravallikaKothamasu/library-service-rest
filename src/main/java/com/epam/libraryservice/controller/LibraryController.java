package com.epam.libraryservice.controller;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.epam.libraryservice.exceptions.AlreadyIssuedException;
import com.epam.libraryservice.exceptions.NoBookFound;
import com.epam.libraryservice.model.IssueBook;
import com.epam.libraryservice.service.LibraryService;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class LibraryController {

	@Autowired
	LibraryService libraryService;

	private static final Logger LOG = LogManager.getLogger(LibraryController.class);

	@ApiOperation(value = "Issues a book to a user")
	@ApiResponses(value = { @ApiResponse(code = 201, message = "Successfully issued the requested book to the user"),
			@ApiResponse(code = 404, message = "If the book is already issued to user or if userId is invalid or if bookId is invalid") })
	@PostMapping("/library/users/{userId}/books/{bookId}")
	public ResponseEntity<IssueBook> issueBookWithBookIdToUser(@PathVariable int userId,
			@PathVariable int bookId) {
		LOG.info("inside /library/users/{userId}/books/{bookId} for issueing book to user");
		try {
		IssueBook issuedBook = libraryService.issueBookToAUser(userId, bookId);
		final java.net.URI uri = ServletUriComponentsBuilder.fromCurrentRequest().path("/{issueId}")
				.buildAndExpand(issuedBook.getIssueId()).toUri();
				return ResponseEntity.created(uri).body(issuedBook);
		}
		catch(AlreadyIssuedException e)
		{
			LOG.info("inside catch of already issued exception");
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST); 
		}
		catch(NoBookFound e) {
			LOG.info("inside catch of No book found exception");
			return new ResponseEntity<>(HttpStatus.NOT_FOUND); 
		}
	}

	@ApiOperation(value = "Deletes a book by given bookId")
	@ApiResponses(value = {
			@ApiResponse(code = 204, message = "Successfully releases a book that user wants to release"),
			@ApiResponse(code = 404, message = "If the book is not issued to user or if userId is invalid or if bookId is invalid") })
	@DeleteMapping("/library/users/{userId}/books/{bookId}")
	public ResponseEntity<Object> releaseBookToUser(@PathVariable int userId, @PathVariable int bookId) {
		LOG.info("inside /library/users/{userId}/books/{bookId} for releasing book to user");
		libraryService.releaseTheBookforUser(userId, bookId);
		return new ResponseEntity<>(HttpStatus.NO_CONTENT);
	}

	@GetMapping("/library/users/{userId}/issuedbooks")
	public ResponseEntity<List<IssueBook>> getBooksIssuedToUser(@PathVariable int userId) {
		LOG.info("inside /library/users/{userId}/issuedbooks");
		return ResponseEntity.ok(libraryService.getBooksIssuedToUser(userId));
		
	}
}
