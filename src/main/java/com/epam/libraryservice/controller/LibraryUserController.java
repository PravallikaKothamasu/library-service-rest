package com.epam.libraryservice.controller;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.epam.libraryservice.interfaces.UserServiceClient;
import com.epam.libraryservice.model.User;
import com.epam.libraryservice.model.UserDTO;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RefreshScope
@RestController
@CrossOrigin(origins = "*", allowedHeaders = "*")
@RequestMapping("/library")
public class LibraryUserController implements UserServiceClient {

	@Autowired
	UserServiceClient userServiceClient;
	
	private static final Logger LOG = LogManager.getLogger(LibraryUserController.class);

	@ApiOperation(value = "Returns All Users")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Successfully retrieves all users") })
	@Override
	public ResponseEntity<List<User>> getAllUsersByLibraryService() {
		LOG.info("inside /library/users to returns all users");
		return userServiceClient.getAllUsersByLibraryService();
	}

	@ApiOperation(value = "Returns user with given userId")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Successfully retrieved user"),
			@ApiResponse(code = 404, message = "User not found with given id") })
	@Override
	public ResponseEntity<User> getUserByIdByLibraryService(int userId) {
		LOG.info("inside /library/users/userId to get a user by userId");
		return userServiceClient.getUserByIdByLibraryService(userId);
	}

	@ApiOperation(value = "Adds a user")
	@ApiResponses(value = { @ApiResponse(code = 201, message = "Successfully created new user") })
	@Override
	public ResponseEntity<User> addUserByLibraryService(UserDTO userDTO) {
		LOG.info("inside /library/users to add a new user");
		return userServiceClient.addUserByLibraryService(userDTO);
	}

	@ApiOperation(value = "Updates a user having given userId")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Successfully updated user with given userId"),
			@ApiResponse(code = 404, message = "User not found with given user id") })
	@Override
	public ResponseEntity<User> updateUserByLibraryService(int userId, UserDTO userDTO) {
		LOG.info("inside /library/users/userId to update a user by userId");
		return userServiceClient.updateUserByLibraryService(userId, userDTO);
	}

	@ApiOperation(value = "Deletes a user with given userId")
	@ApiResponses(value = { @ApiResponse(code = 204, message = "Successfully deleted user with given userId"),
			@ApiResponse(code = 404, message = "User not found with given user id") })
	@Override
	public ResponseEntity<?> deleteUserByLibraryService(int userId) {
		LOG.info("inside /library/users/userId to delete a user by userId");
		return userServiceClient.deleteUserByLibraryService(userId);
	}
}
