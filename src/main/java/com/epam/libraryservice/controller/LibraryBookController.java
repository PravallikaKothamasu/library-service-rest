package com.epam.libraryservice.controller;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.epam.libraryservice.interfaces.BookServiceClient;
import com.epam.libraryservice.model.Book;
import com.epam.libraryservice.model.BookDTO;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RefreshScope
@RestController
@CrossOrigin(origins = "*", allowedHeaders = "*")
@RequestMapping("/library")
public class LibraryBookController implements BookServiceClient {

	@Autowired
	BookServiceClient bookServiceClient;

	private static final Logger LOG = LogManager.getLogger(LibraryBookController.class);

	@ApiOperation(value = "Returns All Books")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Successfully retrieves all books") })
	@Override
	public ResponseEntity<List<Book>> getBooksByLibraryService() {
		LOG.info("inside /library/books to get all books by library service");
		return bookServiceClient.getBooksByLibraryService();
	}

	@ApiOperation(value = "Returns a book by given bookId")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Successfully retrives a book by given bookId"),
			@ApiResponse(code = 404, message = "Book not found with given book id") })
	@Override
	public ResponseEntity<Book> getBookByIdByLibraryService(int bookId) {
		LOG.info("inside /library/books/bookId to get a book by bookId by library service");
		return bookServiceClient.getBookByIdByLibraryService(bookId);
	}

	@ApiOperation(value = "Adds a Book")
	@ApiResponses(value = { @ApiResponse(code = 201, message = "Successfully book is created and added"), })
	@Override
	public ResponseEntity<Book> addBookByLibraryService(BookDTO bookToBeAdded) {
		LOG.info("inside /library/books/ to add a new book by library service");
		return bookServiceClient.addBookByLibraryService(bookToBeAdded);
	}

	@ApiOperation(value = "Deletes a book by given bookId")
	@ApiResponses(value = { @ApiResponse(code = 204, message = "Successfully deletes a book by given bookId"),
			@ApiResponse(code = 404, message = "Book not found with given book id") })
	@Override
	public ResponseEntity<?> deleteBookByIdByLibraryService(int bookId) {
		LOG.info("inside /library/books/bookId to delete a book by bookId by library service");
		return bookServiceClient.deleteBookByIdByLibraryService(bookId);
	}

	@ApiOperation(value = "Updates a book by given bookId and request body")
	@ApiResponses(value = {
			@ApiResponse(code = 204, message = "Successfully uupdates a book by given bookId and given request body"),
			@ApiResponse(code = 404, message = "Book not found with given book id") })
	@Override
	public ResponseEntity<Book> updateBookByLibraryService(int bookId, BookDTO bookToBeUpdated) {
		LOG.info("inside /library/books/bookId to update a book by bookId and given request body by library service");
		return bookServiceClient.updateBookByLibraryService(bookId, bookToBeUpdated);
	}

}
