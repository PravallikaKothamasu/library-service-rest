package com.epam.libraryservice.service;

import java.util.List;
import java.util.stream.Collectors;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.epam.libraryservice.exceptions.AlreadyIssuedException;
import com.epam.libraryservice.exceptions.NoBookFound;
import com.epam.libraryservice.interfaces.BookServiceClient;
import com.epam.libraryservice.interfaces.UserServiceClient;
import com.epam.libraryservice.model.IssueBook;
import com.epam.libraryservice.repositories.IssueBookRepository;

@Service
public class LibraryService {

	@Autowired
	BookServiceClient bookServiceClient;

	@Autowired
	UserServiceClient userServiceClient;

	@Autowired
	IssueBookRepository issueBookRepository;

	private static final Logger LOG = LogManager.getLogger(LibraryService.class);

	public IssueBook issueBookToAUser(int userId, int bookId) {
		LOG.info("inside issueBookToAUser service");
		if (checkIfBookIsNotIssuedToUser(userId, bookId) && isUserPresent(userId) && isBookPresent(bookId)) {
			LOG.info("book with bookId: {} is issued to user with userID: {}", bookId, userId);
			IssueBook bookToBeIssued = new IssueBook();
			bookToBeIssued.setBookId(bookId);
			bookToBeIssued.setUserId(userId);
			return issueBookRepository.save(bookToBeIssued);
		} else {
			IssueBook bookToBeReturnedWhenError = new IssueBook();
			bookToBeReturnedWhenError.setBookId(-1);
			return bookToBeReturnedWhenError;
		}
	}

	public boolean isUserPresent(int userId) {
		int userIdToValidate = userServiceClient.getUserByIdByLibraryService(userId).getBody().getUserId();
		LOG.info("is user present: {}", userIdToValidate == userId);
		return userIdToValidate == userId;

	}

	public boolean isBookPresent(int bookId) {
		int bookIdToValidate = bookServiceClient.getBookByIdByLibraryService(bookId).getBody().getBookId();
		LOG.info("is book present: {}", bookIdToValidate == bookId);
		if(bookIdToValidate == bookId) {
			LOG.info("book with: {} is present",bookId);
			return true;
		}else {
			LOG.info("book with: {} is not present",bookId);
			throw new NoBookFound("book with bookId: "+bookId+" is not found");
		}
	}

	public boolean checkIfBookIsNotIssuedToUser(int userId, int bookId) {
		List<IssueBook> booksOfUser = issueBookRepository.findByUserId(userId);
		List<IssueBook> booksAllreadyIssuedToUser = booksOfUser.stream().filter(book -> book.getBookId() == bookId)
				.collect(Collectors.toList());
		LOG.info("book with {} is issued to user: {}", bookId, !booksAllreadyIssuedToUser.isEmpty());
		if(!booksAllreadyIssuedToUser.isEmpty()) {
			LOG.info("is boook is issued");
			throw new AlreadyIssuedException("book with bookdId: "+bookId+" is already issued");
		}else {
			LOG.info("is boook is not issued");
			return true;
		}
	}

	public void releaseTheBookforUser(int userId, int bookId) {
			List<IssueBook> booksOfUser = issueBookRepository.findByUserId(userId);
			List<IssueBook> bookToBeReleased = booksOfUser.stream().filter(book -> book.getBookId() == bookId)
					.collect(Collectors.toList());
			bookToBeReleased.forEach(book -> issueBookRepository.deleteById(book.getIssueId()));
			LOG.info("book with bookId: {} is released to user with userId: {}", bookId, userId);
	}
	
	public List<IssueBook> getBooksIssuedToUser(int userId) {
		LOG.info("inside getBooksIssuedToUser");
		return issueBookRepository.findByUserId(userId);
	}
}
