package com.epam.libraryservice.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.epam.libraryservice.model.IssueBook;

public interface IssueBookRepository extends JpaRepository<IssueBook, Integer>{
	List<IssueBook> findByUserId(int userId);
}
