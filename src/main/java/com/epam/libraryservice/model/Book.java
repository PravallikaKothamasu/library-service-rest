package com.epam.libraryservice.model;

public class Book {

	int bookId;
	String title;
	String genre;
	Long cost;
	String author;
	
	public Book(int bookId, String title, String genre, Long cost, String author) {
		super();
		this.bookId = bookId;
		this.title = title;
		this.genre = genre;
		this.cost = cost;
		this.author = author;
	}
	public int getBookId() {
		return bookId;
	}
	public void setBookId(int bookId) {
		this.bookId = bookId;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getGenre() {
		return genre;
	}
	public void setGenre(String genre) {
		this.genre = genre;
	}
	public Long getCost() {
		return cost;
	}
	public void setCost(Long cost) {
		this.cost = cost;
	}
	public String getAuthor() {
		return author;
	}
	public void setAuthor(String author) {
		this.author = author;
	}
	@Override
	public String toString() {
		return "Book [bookId=" + bookId + ", title=" + title + "]";
	}
	
	
	
	
}
