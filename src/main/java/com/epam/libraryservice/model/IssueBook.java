package com.epam.libraryservice.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class IssueBook {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	int issueId;
	int userId;
	int bookId;
	
	public IssueBook() {
		
	}
	
	public IssueBook(int issueId, int userId, int bookId) {
		super();
		this.issueId = issueId;
		this.userId = userId;
		this.bookId = bookId;
	}
	public int getIssueId() {
		return issueId;
	}
	public void setIssueId(int issueId) {
		this.issueId = issueId;
	}
	public int getUserId() {
		return userId;
	}
	public void setUserId(int userId) {
		this.userId = userId;
	}
	public int getBookId() {
		return bookId;
	}
	public void setBookId(int bookId) {
		this.bookId = bookId;
	}

	@Override
	public String toString() {
		return "IssueBook [issueId=" + issueId + ", userId=" + userId + ", bookId=" + bookId + "]";
	}
	

}
