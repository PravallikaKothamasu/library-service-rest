package com.epam.libraryservice.util;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import com.epam.libraryservice.model.Book;
import com.epam.libraryservice.model.User;

@Component
public class ResponseEntityUtils {

	private ResponseEntityUtils() {
		
	}
	
	public static ResponseEntity<Book> getServiceUnavailableResponseEntityForBookService() {
		Book bookWhenServiceIsUnavailable = new Book(-1, "unavailable title", "unavailable genre", (long) 00000,
				"unavailable author name");
		return new ResponseEntity<>(bookWhenServiceIsUnavailable, HttpStatus.NOT_FOUND);
	}
	
	public static ResponseEntity<User> getServiceUnavailableResponseEntityForUserService() {
		User userWhenServiceIsUnavailable = new User(-1, "unavailable user name", "unavailable password");
		return ResponseEntity.ok(userWhenServiceIsUnavailable);
	}
}
