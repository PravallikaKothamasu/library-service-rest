package com.epam.libraryservice.fallbackfactory;

import org.springframework.stereotype.Component;

import com.epam.libraryservice.interfaces.UserServiceClient;

import feign.hystrix.FallbackFactory;

@Component
public class UserServiceFallBackFactory implements FallbackFactory<UserServiceClient>{

	@Override
	public UserServiceClient create(Throwable cause) {
		return new UserServiceClientFallback(cause);
	}

}
