package com.epam.libraryservice.fallbackfactory;

import org.springframework.stereotype.Component;

import com.epam.libraryservice.interfaces.BookServiceClient;

import feign.hystrix.FallbackFactory;

@Component
public class BookServiceFallBackFactory implements FallbackFactory<BookServiceClient>{

	@Override
	public BookServiceClient create(Throwable cause) {
		return new BookServiceClientFallBack(cause);
	}

}
