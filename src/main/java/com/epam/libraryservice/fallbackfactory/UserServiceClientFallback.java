package com.epam.libraryservice.fallbackfactory;

import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.epam.libraryservice.interfaces.UserServiceClient;
import com.epam.libraryservice.model.User;
import com.epam.libraryservice.model.UserDTO;
import com.epam.libraryservice.util.ResponseEntityUtils;

import feign.FeignException;

public class UserServiceClientFallback implements UserServiceClient {
	private final Throwable cause;
	private static final Logger LOG = LogManager.getLogger(UserServiceClientFallback.class);
	private static final String DUMMY = "dummy";
	private static final ResponseEntity<User> DUMMY_BOOK =  ResponseEntityUtils.getServiceUnavailableResponseEntityForUserService();

	public UserServiceClientFallback(Throwable cause) {
		this.cause = cause;
	}

	public void handleException() {
		if (cause instanceof FeignException && ((FeignException) cause).status() == 404) {
			LOG.error("404 error took place and requested resource id not available. Error message: {}",
					cause.getLocalizedMessage());
		} else {
			LOG.error("Other error took place: {}", cause.getLocalizedMessage());
		}
	}

	@Override
	public ResponseEntity<List<User>> getAllUsersByLibraryService() {
		handleException();
		List<User> dummyUsers = new ArrayList<>();
		dummyUsers.add(new User(-1, DUMMY, DUMMY));
		LOG.info("{} is returned during the fallback of getAllUsers",dummyUsers);
		return ResponseEntity.ok(dummyUsers);
	}

	@Override
	public ResponseEntity<User> getUserByIdByLibraryService(int userId) {
		handleException();
		LOG.info("{} is returned during the fallback of getUserById",DUMMY_BOOK);
		return DUMMY_BOOK;
	}

	@Override
	public ResponseEntity<User> addUserByLibraryService(UserDTO userDTO) {
		handleException();
		LOG.info("{} is returned during the fallback of addUser",DUMMY_BOOK);
		return DUMMY_BOOK;
	}

	@Override
	public ResponseEntity<User> updateUserByLibraryService(int userId, UserDTO userDTO) {
		handleException();
		LOG.info("{} is returned during the fallback of updateUserById",DUMMY_BOOK);
		return DUMMY_BOOK;
	}

	@Override
	public ResponseEntity<HttpStatus> deleteUserByLibraryService(int userId) {
		handleException();
		return ResponseEntity.ok(HttpStatus.SERVICE_UNAVAILABLE);
	}
}
