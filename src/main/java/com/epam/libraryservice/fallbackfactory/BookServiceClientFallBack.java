package com.epam.libraryservice.fallbackfactory;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.epam.libraryservice.exceptions.AlreadyIssuedException;
import com.epam.libraryservice.interfaces.BookServiceClient;
import com.epam.libraryservice.model.Book;
import com.epam.libraryservice.model.BookDTO;
import com.epam.libraryservice.util.ResponseEntityUtils;

import feign.FeignException;

public class BookServiceClientFallBack implements BookServiceClient {

	private final Throwable cause;
	private static final Logger LOG = LogManager.getLogger(BookServiceClientFallBack.class);
	private static final String DUMMY = "dummy";
	private static final ResponseEntity<Book> DUMMY_BOOK = ResponseEntityUtils
			.getServiceUnavailableResponseEntityForBookService();

	public BookServiceClientFallBack(Throwable cause) {
		this.cause = cause;
	}

	public void handleException() {
		if (cause instanceof FeignException && ((FeignException) cause).status() == 404) {
			LOG.error("404 error took place and requested resource id not available. Error message: {}",
					cause.getLocalizedMessage());
		} else {
			LOG.info(cause.getCause());
			LOG.error("Other error took place: {}", cause.getLocalizedMessage());
		}
	}

	@Override
	public ResponseEntity<List<Book>> getBooksByLibraryService() {
		handleException();
		List<Book> dummyBooks = new ArrayList<>();
		dummyBooks.add(new Book(-1, DUMMY, DUMMY, (long) 000000, DUMMY));
		LOG.error("returns : {}  during fallback of getAllBooks of book-service", dummyBooks);
		return ResponseEntity.ok(dummyBooks);
	}

	@Override
	public ResponseEntity<Book> getBookByIdByLibraryService(int bookId) {
		handleException();
		LOG.error("returns : {}  during fallback of getBookById of book-service", DUMMY_BOOK);
		return DUMMY_BOOK;
	}

	@Override
	public ResponseEntity<Book> addBookByLibraryService(BookDTO bookToBeAdded) {
		handleException();
		LOG.error("returns : {}  during fallback of deleteBook of book-service", DUMMY_BOOK);
		return DUMMY_BOOK;
	}

	@Override
	public ResponseEntity<HttpStatus> deleteBookByIdByLibraryService(int bookId) {
		handleException();
		LOG.info("Service unavailable");
		return ResponseEntity.ok(HttpStatus.SERVICE_UNAVAILABLE);
	}

	@Override
	public ResponseEntity<Book> updateBookByLibraryService(int bookId, BookDTO bookToBeUpdated) {
		handleException();
		LOG.error("returns : {}  during fallback of updateBook of book-service", DUMMY_BOOK);
		return DUMMY_BOOK;
	}

}
