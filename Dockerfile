FROM openjdk:8-jre
COPY target /libraryservice
WORKDIR /libraryservice
ENTRYPOINT ["java", "-jar", "libraryservice-0.0.1-SNAPSHOT.jar"]